import subprocess
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

PORT = 9222
PROFILE = r'C:\remote-profile'

if __name__ == "__main__":
    cmd = f'netstat -ano | findstr 127.0.0.1:{PORT} | findstr LISTENING'
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    out, err = proc.communicate()

    # out 정보가 없다는건 9222 포트를 사용중인 프로그램이 없다는 뜻.
    if len(out) <= 0:
        cmd = r'C:\Program Files\Google\Chrome\Application\chrome.exe'
        cmd += f' --user-data-dir={PROFILE} --remote-debugging-port={PORT}'
        subprocess.Popen(cmd) # 크롬 실행
        
    # 크롬 드라이버 생성
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option('debuggerAddress', f'127.0.0.1:{PORT}')
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), 
                            options=chrome_options)
    driver.get('https://www.naver.com') # 네이버로 이동
